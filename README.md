# Mingyuanyun OpenAPI SDK Core for PHP

[![Latest Stable Version](https://poser.pugx.org/mingyuanyun/openapi-sdk-php-core/v/stable)](https://packagist.org/packages/mingyuanyun/openapi-sdk-php-core)
[![Total Downloads](https://poser.pugx.org/mingyuanyun/openapi-sdk-php-core/downloads)](https://packagist.org/packages/mingyuanyun/openapi-sdk-php-core)
[![License](https://poser.pugx.org/mingyuanyun/openapi-sdk-php-core/license)](https://packagist.org/packages/mingyuanyun/openapi-sdk-php-core)

OpenAPI SDK PHP Core 是基于 PHP 语言帮助开发者管理凭证、发起服务请求的客户端工具，同时为 OpenAPI SDK PHP 提供底层核心支持

---  

## 安装依赖

Mingyuanyun OpenAPI SDK Core for PHP 的安装需要依赖到[全局安装 Composer](https://getcomposer.org/doc/00-intro.md#globally)，如果你的系统已经准备就绪，可以在项目目录下运行以下命令来拉取依赖：

```
composer require mingyuanyun/openapi-sdk-php-core
```

> 如果你的 Composer 版本是 2.0 以上，为了避免本地环境与运行环境 PHP 版本不一致，可以加上 --ignore-platform-reqs 参数来避免本地 PHP 版本约束

---  

## 快速使用

Mingyuanyun OpenAPI SDK Core for PHP 的使用分两种场景，分别是：  
- 被具体开放服务子类继承封装后对外提供便捷的服务接口调用  
- 快速发起自定义的接口请求  

### 便捷的开放服务接口调用

详情用法可查看[明源开放服务 SDK](https://opengit.mysre.cn/opensdk/openapi-sdk-php)

### 快速发起自定义请求

发起一个请求主要用到通用的 `Client` 客户端类，示例如下：

```php
<?php

use Mingyuanyun\Core\Client;
use Mingyuanyun\Core\Request;

$request = new Request('GET', 'https://www.domain.com/get');
$client = new Client($request);

$response = $client->invoke();

if ($response->isSuccess()) {
    // 数据处理
} else {
    // 异常处理
}
```

---  

## 发行日志

请查看[发行说明](/CHANGELOG.md)。