<?php

namespace Mingyuanyun\Core;

use Mingyuanyun\Core\Support\Helper\StringHelper;

/**
 * 通用客户端类
 */
class Client implements ClientInterface
{
    /**
     * 服务请求数据类
     *
     * @var Request
     */
    protected $request;

    /**
     * 客户端构造器
     *
     * @param Request $request 标准请求数据类
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Response
     */
    public function invoke()
    {
        return $this->resolveRequest()->resolveResponse();
    }

    /**
     * 发起服务调用，返回相应数据。每次调用都会发起新的请求
     *
     * @return $this
     */
    private function resolveRequest()
    {
        $this->request->setHeaders([
            StringHelper::hk('request-datetime')  => StringHelper::dateTime(),
            StringHelper::hk('request-timestamp') => time(),
        ])->pending();

        return $this;
    }

    /**
     * 使用发送器发送请求
     *
     * @return Response
     */
    private function resolveResponse()
    {
        return SDK::getSenderInstance()->send($this->request);
    }
}
