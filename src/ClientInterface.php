<?php

namespace Mingyuanyun\Core;

/**
 * 客户端接口
 */
interface ClientInterface
{
    /**
     * 发起请求
     *
     * @return ResponseInterface
     */
    public function invoke();
}
