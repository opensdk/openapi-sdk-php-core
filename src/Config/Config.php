<?php

namespace Mingyuanyun\Core\Config;

use Mingyuanyun\Core\Support\Helper\ArrayHelper;
use Mingyuanyun\Core\Validator\ConfigValidator;

/**
 * 开放服务 SDK 配置
 */
class Config
{
    /** 默认 Debug 模式开关状态 */
    const DEFAULT_DEBUG = false;

    /** 请求唯一 ID 前缀 */
    const REQUEST_ID_PREFIX = 'myy_opensdk_';

    /**
     * 开放服务访问密钥 ID
     *
     * @var string
     */
    public $secretId;

    /**
     * 开放服务访问密钥 Key
     *
     * @var string
     */
    public $secretKey;

    /**
     * 是否开启 Debug
     */
    public $debug;

    /**
     * 服务配置
     *
     * @var ServerConfig
     */
    public $server;

    /**
     * 代理配置
     *
     * @var ProxyConfig
     */
    public $proxy;

    /**
     * 配置构造器
     */
    public function __construct(array $configs)
    {
        $secretId  = ArrayHelper::getValue($configs, 'secretId', '');
        $secretKey = ArrayHelper::getValue($configs, 'secretKey', '');
        $debug     = ArrayHelper::getValue($configs, 'debug', static::DEFAULT_DEBUG);

        ConfigValidator::secretId($secretId);
        ConfigValidator::secretKey($secretKey);
        ConfigValidator::debug($debug);

        $this->secretId  = $secretId;
        $this->secretKey = $secretKey;
        $this->debug     = $debug;

        $this->server = new ServerConfig(ArrayHelper::getValue($configs, 'server', []));
        $this->proxy  = new ProxyConfig(ArrayHelper::getValue($configs, 'proxy', []));
    }

    /**
     * 加载数组数据到配置类属性上
     *
     * @param array $configs 配置数据数组
     * @return $this
     */
    public static function load(array $configs)
    {
        return new Config($configs);
    }
}
