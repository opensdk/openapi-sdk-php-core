<?php

namespace Mingyuanyun\Core\Config;

use Mingyuanyun\Core\Support\Helper\ArrayHelper;
use Mingyuanyun\Core\Validator\ConfigValidator;

/**
 * 代理地址配置类
 */
class ProxyConfig
{
    /**
     * 请求代理地址
     *
     * @var string
     */
    public $proxy;

    /**
     * 专门用于 HTTP 协议请求的代理地址
     *
     * @var string
     */
    public $httpProxy;

    /**
     * 专门用于 HTTPS 协议请求的代理地址
     *
     * @var string
     */
    public $httpsProxy;

    /**
     * 不使用代理的请求白名单
     *
     * @var string[]
     */
    public $whiteList = [];

    /**
     * 代理配置构造器
     */
    public function __construct(array $configs)
    {
        $proxy      = ArrayHelper::getValue($configs, 'proxy', '');
        $httpProxy  = ArrayHelper::getValue($configs, 'httpProxy', '');
        $httpsProxy = ArrayHelper::getValue($configs, 'httpsProxy', '');
        $whiteList  = ArrayHelper::getValue($configs, 'whiteList', []);

        if (!empty($proxy)) {
            ConfigValidator::proxy($proxy);
        }
        if (!empty($httpProxy)) {
            ConfigValidator::httpProxy($httpProxy);
        }
        if (!empty($httpsProxy)) {
            ConfigValidator::httpsProxy($httpsProxy);
        }
        if (!empty($whiteList)) {
            ConfigValidator::proxyWhiteList($whiteList);
        }

        $this->proxy      = $proxy;
        $this->httpProxy  = $httpProxy;
        $this->httpsProxy = $httpsProxy;
        $this->whiteList  = $whiteList;
    }
}
