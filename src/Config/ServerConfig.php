<?php

namespace Mingyuanyun\Core\Config;

use Mingyuanyun\Core\Support\Helper\ArrayHelper;
use Mingyuanyun\Core\Validator\ConfigValidator;

/**
 * 服务配置
 */
class ServerConfig
{
    /** 默认连接超时时长 */
    const DEFAULT_CONNECT_TIMEOUT = 5.0;

    /** 默认等待响应超时时长 */
    const DEFAULT_TIMEOUT = 10.0;

    /** 默认是否校验证书开关 */
    const DEFAULT_VERIFY = true;

    /**
     * 服务地址
     *
     * @var string
     */
    public $host;

    /**
     * 建立连接超时时长，单位：秒
     *
     * @var float
     */
    public $connectTimeout;

    /**
     * 等待响应超时时长，单位：秒
     *
     * @var float
     */
    public $timeout;

    /**
     * 是否校验证书
     *
     * @var bool
     */
    public $verify;

    /**
     * 服务配置构造器
     */
    public function __construct(array $configs)
    {
        $host           = ArrayHelper::getValue($configs, 'host', '');
        $connectTimeout = ArrayHelper::getValue($configs, 'connectTimeout', static::DEFAULT_CONNECT_TIMEOUT);
        $timeout        = ArrayHelper::getValue($configs, 'timeout', static::DEFAULT_TIMEOUT);
        $verify         = ArrayHelper::getValue($configs, 'verify', static::DEFAULT_VERIFY);

        ConfigValidator::serverHost($host);
        ConfigValidator::serverVerify($verify);
        if (!empty($connectTimeout)) {
            ConfigValidator::serverConnectTimeout($connectTimeout);
        }
        if (!empty($timeout)) {
            ConfigValidator::serverTimeout($timeout);
        }

        $this->host           = $host;
        $this->connectTimeout = $connectTimeout;
        $this->timeout        = $timeout;
        $this->verify         = $verify;
    }
}
