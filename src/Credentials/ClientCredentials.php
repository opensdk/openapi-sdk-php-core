<?php

namespace Mingyuanyun\Core\Credentials;

use Mingyuanyun\Core\CredentialsInterface;
use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Core\Validator\CredentialsValidator;

/**
 * OAuth2.0 client_credentials 访问鉴权凭证类
 *
 * @method string getSecretId()     return secret id.
 * @method string getSecretKey()    return secret key.
 * @method string getToken()        return access token.
 */
class ClientCredentials implements CredentialsInterface
{
    use AccessTrait;

    /**
     * @var string
     */
    private $secretId;

    /**
     * @var string
     */
    private $secretKey;

    /**
     * @var string
     */
    private $token;

    /**
     * 访问凭证构造器
     *
     * @param string $secretId  密钥 ID
     * @param string $secretKey 密钥 Key
     * @param string $token     访问令牌
     */
    public function __construct($secretId, $secretKey, $token = null)
    {
        CredentialsValidator::secret($secretId, $secretKey);

        $this->secretId  = $secretId;
        $this->secretKey = $secretKey;

        if (!is_null($token)) {
            $this->setToken($token);
        }
    }

    /**
     * 快速构造一个拥有基础属性的 Credentials 凭证类
     *
     * @param string $secretId  密钥 ID
     * @param string $secretKey 密钥 Key
     * @param string $token     访问令牌
     * @return $this
     */
    public static function make($secretId, $secretKey, $token = null)
    {
        return new static($secretId, $secretKey, $token);
    }

    /**
     * 设置访问凭证关联的访问令牌
     *
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        CredentialsValidator::token($token);
        $this->token = $token;
        return $this;
    }
}
