<?php

namespace Mingyuanyun\Core\Credentials;


class Credentials
{
    /**
     * @return TodoCredentials
     */
    public static function todo()
    {
        return new TodoCredentials;
    }

    /**
     * @param string $secretId
     * @param string $secretKey
     * @param string $token
     * @return ClientCredentials
     */
    public static function client($secretId, $secretKey, $token = null)
    {
        return new ClientCredentials($secretId, $secretKey, $token);
    }
}
