<?php

namespace Mingyuanyun\Core\Exception;

/**
 * SDK 参数异常类
 */
class InvalidArgumentException extends MYYSDKException
{
}
