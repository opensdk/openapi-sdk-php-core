<?php

namespace Mingyuanyun\Core\Exception;

/**
 * SDK 异常基类
 */
class MYYSDKException extends \RuntimeException
{
}
