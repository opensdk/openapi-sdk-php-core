<?php

namespace Mingyuanyun\Core;

use GuzzleHttp\RequestOptions as Options;
use Mingyuanyun\Core\Support\Helper\StringHelper;

class HttpSender extends Sender
{
    protected function resolveOptions()
    {
        $options = [];

        $options[Options::QUERY]   = $this->resolveQuery();
        $options[Options::HEADERS] = $this->resolveHeaders();

        $body = $this->resolveBody();
        if ($this->request->isJsonFormat()) {
            $options[Options::JSON] = $body;
        } elseif ($this->request->isFormFormat()) {
            $options[Options::FORM_PARAMS] = $body;
        }

        $this->setOptions($options);
    }

    /**
     * @return array
     */
    private function resolveHeaders()
    {
        $headers = $this->request->getHeaders();

        $headers[StringHelper::hk('request-type')] = 'http';

        return $headers;
    }

    /**
     * @return array
     */
    private function resolveQuery()
    {
        return $this->request->getQuery();
    }

    /**
     * @return array
     */
    private function resolveBody()
    {
        return $this->request->getBody();
    }
}
