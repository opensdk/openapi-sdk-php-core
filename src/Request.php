<?php

namespace Mingyuanyun\Core;

use GuzzleHttp\RequestOptions as Options;
use Mingyuanyun\Core\Config\Config;
use Mingyuanyun\Core\Exception\InvalidArgumentException;
use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\Support\Helper\ArrayHelper;
use Mingyuanyun\Core\Support\Helper\StringHelper;
use Mingyuanyun\Core\Support\LimitRule;
use Mingyuanyun\Core\Support\RequestOptions;
use Mingyuanyun\Core\Support\RetryRule;
use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Core\Validator\RequestValidator;

/**
 * 请求数据类
 *
 * @method RetryRule getRetryRule() return field $retryRule value.
 */
class Request implements RequestInterface
{
    use AccessTrait;

    /**
     *
     * 请求状态说明：
     * 1. 处于已就绪状态的请求，不允许修改请求数据，也不能更新重试次数等信息
     * 2. 处于发送中状态的请求，不允许修改请求数据，但可以更新重试次数的数据
     * 3. 处于已完成状态的请求，不允许修改任何数据
     */

    /** 请求初始化 */
    const INIT = 0;

    /** 请求已就绪 */
    const PENDING = 1;

    /** 请求发送中 */
    const SENDING = 2;

    /** 请求已完成 */
    const COMPLETE = 3;

    /**
     * 请求唯一 ID
     *
     * @var string
     */
    private $id;

    /**
     * 请求域名
     *
     * @var string
     */
    private $host;

    /**
     * 请求地址
     *
     * @var string
     */
    protected $uri;

    /**
     * 请求方法
     *
     * @var string
     */
    protected $method;

    /**
     * 请求查询数组
     *
     * @var array
     */
    private $query;

    /**
     * 请求体数组
     *
     * @var array
     */
    private $body;

    /**
     * 请求头数组
     *
     * @var array
     */
    private $headers;

    /**
     * @var array
     */
    private $cookies;

    /**
     * 当前请求是第几次客户端异常引起的重试
     *
     * @var int
     */
    private $clientRetryTimes = 0;

    /**
     * 当前请求是第几次服务端异常引起的重试
     *
     * @var int
     */
    private $serverRetryTimes = 0;

    /**
     * 建立连接超时时间，单位：秒
     *
     * @var float
     */
    private $connectTimeout;

    /**
     * 等待超时时间，单位：秒
     *
     * @var float
     */
    private $timeout;

    /**
     * 请求体编码类型
     *
     * @var string JSON|FORM
     */
    private $format;

    /**
     * 是否进行证书校验
     *
     * @var bool
     */
    private $verify;

    /**
     * @var RetryRule
     */
    private $retryRule;

    /**
     * @var LimitRule
     */
    private $limitRule;

    /**
     * 请求类状态
     *
     * @var int
     */
    private $status = self::INIT;

    /**
     * 标准请求构造器
     *
     * @param string $method    请求方式
     * @param string $uri       请求接口路径
     * @param array  $query     请求查询数据数组
     * @param array  $body      请求体数据数组
     * @param array  $headers   请求头数据数组
     */
    public function __construct(
        $method = '',
        $uri = '',
        array $query = [],
        array $body = [],
        array $headers = []
    ) {
        $this->init();

        !empty($uri) && $this->setUri($uri);
        !empty($method) && $this->setMethod($method);

        !empty($query) && $this->setQuery($query);
        !empty($body) && $this->setBody($body);
        !empty($headers) && $this->setHeaders($headers);

        $this->retryRule = new RetryRule();
        $this->limitRule = new LimitRule();

    }

    /**
     * 初始化请求数据
     */
    private function init()
    {
        $this->id = StringHelper::generateId(Config::REQUEST_ID_PREFIX);

        $this->headers = [
            StringHelper::hk('version')    => SDK::VERSION,
            StringHelper::hk('request-id') => $this->id,
        ];

        if (SDK::hasConfig()) {
            $this->headers[StringHelper::hk('secret-id')] = SDK::config()->secretId;
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getConnectTimeout()
    {
        return $this->connectTimeout;
    }

    /**
     * @return float
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @return bool
     */
    public function getVerify()
    {
        return $this->verify;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return array
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setUri($uri)
    {
        RequestValidator::changeProps($this);
        RequestValidator::uri($uri);
        $this->uri = $uri;
        return $this;
    }

    /**
     * 设置请求方式。支持的值有 GET|POST|PUT|DELETE
     *
     * @param string $method 请求方式
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setMethod($method)
    {
        RequestValidator::changeProps($this);
        RequestValidator::method($method);
        $this->method = $method;
        return $this;
    }

    /**
     * 设置请求查询数据
     *
     * @param array $query 请求查询数据
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setQuery(array $query)
    {
        RequestValidator::changeProps($this);
        RequestValidator::query($query);

        if (!empty($this->query)) {
            $this->query = ArrayHelper::merge($this->query, $query);
        } else {
            $this->query = $query;
        }

        return $this;
    }

    /**
     * 设置请求头数据
     *
     * @param array $headers 请求头数据
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setHeaders(array $headers)
    {
        RequestValidator::changeProps($this);
        RequestValidator::headers($headers);

        if (!empty($this->headers)) {
            $this->headers = ArrayHelper::merge($this->headers, $headers);
        } else {
            $this->headers = $headers;
        }

        return $this;
    }

    /**
     * 设置请求体数据
     *
     * @param array $body 请求体数据
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setBody(array $body)
    {
        RequestValidator::changeProps($this);
        RequestValidator::body($body);

        if (!empty($this->body)) {
            $this->body = ArrayHelper::merge($this->body, $body);
        } else {
            $this->body = $body;
        }

        return $this;
    }

    /**
     * 设置 Cookies 数据
     *
     * @param array $cookies
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setCookies(array $cookies)
    {
        RequestValidator::changeProps($this);
        RequestValidator::cookies($cookies);

        if (!empty($this->cookies)) {
            $this->cookies = ArrayHelper::merge($this->cookies, $cookies);
        } else {
            $this->cookies = $cookies;
        }

        return $this;
    }

    /**
     * 设置连接超时时长
     *
     * @param float $sec
     * @throws InvalidArgumentException
     */
    public function setConnectTimeout($sec)
    {
        RequestValidator::connectTimeout($sec);
        $this->connectTimeout = $sec;
        return $this;
    }

    /**
     * 设置响应超时时长
     *
     * @param float $sec
     * @throws InvalidArgumentException
     */
    public function setTimeout($sec)
    {
        RequestValidator::timeout($sec);
        $this->timeout = $sec;
        return $this;
    }

    /**
     * 设置数据编码格式
     *
     * @param string $format
     * @throws InvalidArgumentException
     */
    public function setFormat($format)
    {
        RequestValidator::format($format);
        $this->format = $format;
        return $this;
    }

    /**
     * 设置请求是否需要校验证书
     *
     * @param bool $verify 是否校验证书
     * @return $this
     * @throws InvalidArgumentException
     */
    public function setVerify($verify)
    {
        $this->verify = $verify;
        return $this;
    }

    /**
     * 设置请求重试规则
     *
     * @param RetryRule $rule 重试规则
     * @return $this
     */
    public function setRetryRule(RetryRule $rule)
    {
        $this->retryRule = $rule;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * 将请求状态变更为已就绪
     *
     * @throws RequestException
     */
    public function pending()
    {
        if ($this->status === self::PENDING) {
            return;
        }

        RequestValidator::uri($this->uri);
        RequestValidator::method($this->method);

        $this->status = self::PENDING;
    }

    /**
     * 将请求状态变更为发送中
     *
     * @throws RequestException
     */
    public function sending()
    {
        if ($this->status === self::SENDING) {
            return;
        }
        if ($this->status !== self::PENDING) {
            throw new RequestException('Want change request status to sending, current must be pending.');
        }
        $this->status = self::SENDING;
    }

    /**
     * 将请求状态变更为已完成
     *
     * @throws RequestException
     */
    public function complete()
    {
        if ($this->status === self::COMPLETE) {
            return;
        }
        if ($this->status !== self::SENDING) {
            throw new RequestException('Want change request status to complete, current must be sending.');
        }
        $this->status = self::COMPLETE;
    }

    /**
     * 重置请求，比如将请求的状态重置为已就绪
     */
    public function reset()
    {
        unset($this->headers[StringHelper::hk('request-datetime')]);
        unset($this->headers[StringHelper::hk('request-timestamp')]);

        $this->status = self::INIT;
    }

    /**
     * 判断请求的数据编码格式是否 Json
     *
     * @return bool
     */
    public function isJsonFormat()
    {
        return $this->format === RequestOptions::FORMAT_JSON;
    }

    /**
     * 判断请求的数据编码格式是否 Form 表单方式
     *
     * @return bool
     */
    public function isFormFormat()
    {
        return $this->format === RequestOptions::FORMAT_FORM;
    }

    /**
     * @return int[]
     */
    public function getClientRetryCodes()
    {
        return $this->retryRule->getClientRetryCodes();
    }

    /**
     * @return string[]
     */
    public function getClientRetryStrings()
    {
        return $this->retryRule->getClientRetryStrings();
    }

    /**
     * @return int[]
     */
    public function getServerRetryCodes()
    {
        return $this->retryRule->getServerRetryCodes();
    }

    /**
     * @return string[]
     */
    public function getServerRetryStrings()
    {
        return $this->retryRule->getServerRetryStrings();
    }

    /**
     * @return string
     */
    public function toString()
    {
        return '';
    }
}
