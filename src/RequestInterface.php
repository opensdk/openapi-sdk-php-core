<?php

namespace Mingyuanyun\Core;

/**
 * 请求接口类
 */
interface RequestInterface
{
    /**
     * 获取请求唯一 ID
     *
     * @return string
     */
    public function getId();

    /**
     * 获取接口地址路径
     *
     * @return string
     */
    public function getUri();

    /**
     * 设置接口请求地址路径
     * 
     * @param string $uri 接口路径
     * @return static
     */
    public function setUri($uri);

    /**
     * 获取请求方式：GET|POST|PUT|DELETE
     *
     * @return string
     */
    public function getMethod();

    /**
     * 设置请求方式。支持的值有 GET|POST|PUT|DELETE
     * 
     * @param string $method 请求方式
     * @return static
     */
    public function setMethod($method);

    /**
     * 获取请求查询参数
     *
     * @return array
     */
    public function getQuery();

    /**
     * 设置请求查询数据
     * 
     * @param array $query 查询数据数组
     * @return static
     */
    public function setQuery(array $query);

    /**
     * 获取请求体数据
     *
     * @return array
     */
    public function getBody();

    /**
     * 设置请求体数据
     * 
     * @param array $body 请求体数组
     * @return static
     */
    public function setBody(array $body);

    /**
     * 获取请求头数据
     *
     * @return array
     */
    public function getHeaders();

    /**
     * 设置请求头数据
     * 
     * @param array $headers 请求头数组
     * @return static
     */
    public function setHeaders(array $headers);

    /**
     * 获取 Cookie 数据
     *
     * @return array
     */
    public function getCookies();

    /**
     * 设置请求 Cookie 数据
     * 
     * @param array $cookies Cookie 数组
     * @return static
     */
    public function setCookies(array $cookies);

    /**
     * 获取请求设置的建立连接超时时间，单位：秒
     *
     * @return float
     */
    public function getConnectTimeout();

    /**
     * 获取请求设置的响应等待超时时间，单位：秒
     *
     * @return float
     */
    public function getTimeout();

    /**
     * 获取请求设置的证书校验校验开关
     *
     * @return bool
     */
    public function getVerify();

    /**
     * 将请求数据格式化成字符串
     *
     * @return string
     */
    public function toString();

    /**
     * 遇到客户端异常时，需要进行自动重试的响应状态码清单
     * 根据客户端抛出的异常状态码，匹配到清单中的状态码时会触发重试
     *
     * @return int[]
     */
    public function getClientRetryCodes();

    /**
     * 遇到客户端异常时，需要进行自动重试的关键内容清单
     * 从客户端的异常信息中，匹配到清单中内容时会触发重试
     *
     * @return string[]
     */
    public function getClientRetryStrings();

    /**
     * 遇到服务端异常时，需要进行自动重试的响应状态码清单
     * 根据服务端返回的 Response Status Code，匹配到清单中的状态码时会触发重试
     *
     * @return int[]
     */
    public function getServerRetryCodes();

    /**
     * 遇到服务端异常时，需要进行自动重试的关键内容清单
     * 从服务端返回的失败信息中，匹配到清单中内容时会触发重试
     *
     * @return string[]
     */
    public function getServerRetryStrings();

    /**
     * 判断请求的数据编码格式是否 JSON
     *
     * @return bool
     */
    public function isJsonFormat();

    /**
     * 判断请求的数据编码格式是否 Form 表单方式
     *
     * @return bool
     */
    public function isFormFormat();

    /**
     * 设置数据编码格式
     * 
     * @param string $format JSON|FORM
     * @return static
     */
    public function setFormat($format);

    /**
     * 获取请求数据类当前所处的状态
     * 
     * @return int
     */
    public function getStatus();

    /**
     * 请求状态变更为：已就绪
     */
    public function pending();

    /**
     * 请求状态变更为：发送中
     */
    public function sending();

    /**
     * 请求状态变更为：已完成
     */
    public function complete();

    /**
     * 重置请求
     */
    public function reset();
}
