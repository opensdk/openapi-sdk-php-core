<?php

namespace Mingyuanyun\Core;

use Mingyuanyun\Core\Exception\ResponseException;
use Mingyuanyun\Core\Support\Helper\ArrayHelper;
use Mingyuanyun\Core\Support\Helper\StringHelper;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * 响应数据类
 */
class Response implements ResponseInterface
{
    /** 错误码默认字段名 */
    const DEFAULT_ERRCODE_FIELD = 'code';

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var PsrResponseInterface
     */
    protected $guzzleResponse;

    /**
     * 响应数据体内容原文
     *
     * @var string
     */
    protected $bodyString = '';

    /**
     * 响应数据体数据
     *
     * @var array
     */
    protected $body = [];

    /**
     * 响应头数据数组
     *
     * @var array
     */
    protected $headers = [];

    /**
     * 响应状态码
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * 客户端异常引起的重试次数
     *
     * @var int
     */
    protected $clientRetryTimes = 0;

    /**
     * 服务端异常引起的重试次数
     *
     * @return int
     */
    protected $serverRetryTimes = 0;

    /**
     * 重试记录快照
     *
     * @var string[]
     */
    protected $retrySnapshots = [];

    /**
     * 个性化指定的返回数据中错误码的属性名
     * 
     * @var string
     */
    protected $errcodeField;

    /**
     * 构造器
     *
     * @param Request $request 产生该响应的请求类
     * @param PsrResponseInterface $guzzleResponse 底层 Guzzle 扩展返回的 Response 实例
     */
    final public function __construct(Request $request, PsrResponseInterface $guzzleResponse = null)
    {
        $this->request        = $request;
        $this->guzzleResponse = $guzzleResponse;

        $this->unmarshal();
    }

    /**
     * 将核心响应类 GuzzleResponse 中的数据整理到当前类属性中
     */
    final protected function unmarshal()
    {
        if (!$this->hasGuzzleResponse()) {
            return;
        }

        $this->statusCode = $this->guzzleResponse->getStatusCode();
        $this->bodyString = (string) $this->guzzleResponse->getBody();
        $this->body       = @json_decode($this->bodyString, true) ?? [];

        $tmpHeaders = $this->guzzleResponse->getHeaders();
        foreach ($tmpHeaders as $k => $vs) {
            $this->headers[$k] = implode(', ', $vs);
        }
    }

    /**
     * 根据请求响应的结果，判断业务请求是否成功。
     * 1. 如果 $soEasy 的值为 false，只会判断当前是否拿到 GuzzleHttp 的实例，以及接口响应的状态码是否为 200，如果没有实例或者状态码
     * 不等于 200，将会返回 false。
     * 2. 如果 $soEasy 的值为 true，当 GuzzleHttp Response 实例存在切响应状态码为 200 时。将会尝试判断请求体 Body 中的数据内容是
     * 否正确。如果根据 Content-Type 判断到数据为 Json 格式，会尝试判断返回数据中的 errcode 错误码是否为指定的 $successCode 值，
     * 相同则返回 true，不同则返回 false。其次如果响应数据不是 Json 编码格式的内容，将直接判断 Body 中是否有数据返回，有数据则返回
     * true，没数据则返回 false
     *
     * @param  bool $soEasy 是否进行更多的数据业务校验，简化调用方需要进行的判断逻辑编写。让调用方判断接口是否调用成功更简单
     * @param  int  $okCode 如果返回数据是 Json 编码格式且其中含有 errcode 命名的错误码字段时，判断该错误码为”成功“的值。默认 0
     * @return bool
     */
    public function isSuccess($soEasy = false, $okCode = self::ERRCODE_OK)
    {
        if (!$this->hasGuzzleResponse()) {
            return false;
        }
        if ($this->getStatusCode() >= 300) {
            return false;
        }

        if (!$soEasy) {
            return true;
        }
        if ($this->isJson()) {
            $code = $this->getBody($this->getErrCodeField());
            return !is_null($code) && $code == $okCode;
        }
        return !empty($this->toString());
    }

    /**
     * isSuccess(true) 调用方式的别名
     * 
     * @return bool
     */
    public function isSuccessPro()
    {
        return $this->isSuccess(true);
    }

    /**
     * 获取请求响应的 HTTP 状态码。如果请求异常，则默认返回 0
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * 获取请求所有的响应头数据。在这里会对 GuzzleHttp 的 Headers 数据格式做二次处理。
     * GuzzleHttp 默认返回的 Headers 都是 stringp[][] 的格式，正常情况下出现这种数据
     * 的可能性少之又少，因此统一转换成 map[string]string（'key' => 'value'） 的格式。
     * 如果遇到有多个相同 Key 的请求头数据的时候，默认取最后一个的数据值。
     * 如果需要获取 string[][] 的请求头数据，可以通过 getGuzzleResponse 自行处理
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * 根据提供的请求头键名，获取对应的请求头数据内容。如果内容不存在，会返回 $default 的值
     *
     * @param  string $key      请求头键名
     * @param  string $default  找不到数据时的默认返回值
     * @return string
     */
    public function getHeader($key, $default = '')
    {
        return ArrayHelper::getValue($this->headers, $key, $default);
    }

    /**
     * 判断响应数据是否 Json 编码格式。通过判断 Content-Type 中是否含有 application/json 来确定
     *
     * @return bool
     */
    public function isJson()
    {
        return StringHelper::contains($this->getHeader('Content-Type'), 'application/json');
    }

    /**
     * 获取响应数据 Body 的数据流实例
     *
     * @param string $key       数据键名
     * @param mixed  $default   找不到数据时的默认值
     * @return mixed
     */
    public function getBody($key, $default = null)
    {
        return ArrayHelper::getValue($this->body, $key, $default);
    }

    /**
     * 将响应的数据 Body 内容以字符串的形式返回
     *
     * @return string
     */
    public function toString()
    {
        return $this->bodyString;
    }

    /**
     * 将响应的数据 Body 内容转换成数组格式，如果转换失败，将返回空数组
     *
     * @return array
     */
    public function toArray()
    {
        return $this->body;
    }

    /**
     * 将响应的数据 Body 内容转成对象，如果转换失败，将返回 null
     *
     * @return object|null
     */
    public function toObject()
    {
        return @json_decode($this->bodyString) ?? null;
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * 获取原生 GuzzleHttp Response 实例
     *
     * @return ResponseInterface
     */
    public function getGuzzleResponse()
    {
        return $this->guzzleResponse;
    }

    /**
     * 是否持有一个可用的 GuzzleHttp Response 类实例
     *
     * @return bool
     */
    protected function hasGuzzleResponse()
    {
        return !is_null($this->guzzleResponse);
    }

    /**
     * @param string[] $snapshots
     * @param int $clientRetryTimes
     * @param int $serverRetryTimes
     */
    public function setRetryInfo($snapshots, $clientRetryTiems, $serverRetryTimes)
    {
        $this->retrySnapshots   = $snapshots;
        $this->clientRetryTimes = $clientRetryTiems;
        $this->serverRetryTimes = $serverRetryTimes;
    }

    /**
     * @return int
     */
    public function getClientRetryTimes()
    {
        return $this->clientRetryTimes;
    }

    /**
     * @return int
     */
    public function getServerRetryTimes()
    {
        return $this->serverRetryTimes;
    }

    /**
     * 设置错误码的字段名
     * 
     * @param string $field 字段名
     * @return void
     */
    public function setErrCodeField($field)
    {
        if (!empty($field) && !is_string($field)) {
            throw new ResponseException('errcode field must be a string.');
        }
        $this->errcodeField = $field;
    }

    /**
     * 获取错误码的字段名
     * 
     * @return string
     */
    public function getErrCodeField()
    {
        return $this->errcodeField ?: self::DEFAULT_ERRCODE_FIELD;
    }
}
