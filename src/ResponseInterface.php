<?php

namespace Mingyuanyun\Core;

/**
 * 响应数据接口类
 *
 * @codeCoverageIgnore
 */
interface ResponseInterface
{
    /** 接口返回成功的错误码值：0 */
    const ERRCODE_OK = 0;

    /**
     * @return int
     */
    public function getStatusCode();

    /**
     * 根据请求响应的结果，判断业务请求是否成功。
     * 1. 如果 $soEasy 的值为 false，只会判断当前是否拿到 GuzzleHttp 的实例，以及接口响应的状态码是否为 200，如果没有实例或者状态码
     * 不等于 200，将会返回 false。
     * 2. 如果 $soEasy 的值为 true，当 GuzzleHttp Response 实例存在切响应状态码为 200 时。将会尝试判断请求体 Body 中的数据内容是
     * 否正确。如果根据 Content-Type 判断到数据为 Json 格式，会尝试判断返回数据中的 errcode 错误码是否为指定的 $successCode 值，
     * 相同则返回 true，不同则返回 false。其次如果响应数据不是 Json 编码格式的内容，将直接判断 Body 中是否有数据返回，有数据则返回
     * true，没数据则返回 false
     *
     * @param  bool $soEasy 是否进行更多的数据业务校验，简化调用方需要进行的判断逻辑编写。让调用方判断接口是否调用成功更简单
     * @param  int  $okCode 如果返回数据是 Json 编码格式且其中含有 errcode 命名的错误码字段时，判断该错误码为”成功“的值。默认 0
     * @return bool
     */
    public function isSuccess($soEasy = false, $okCode = self::ERRCODE_OK);

    /**
     * 获取响应所对应的请求数据类
     *
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * 设置响应相关的重试信息
     *
     * @param string[] 每次重试的数据快照
     * @param int 客户端重试次数
     * @param int 服务端重试次数
     */
    public function setRetryInfo($snapshots, $clientRetryTiems, $serverRetryTimes);

    /**
     * 获取客户端异常引起的重试次数
     *
     * @return int
     */
    public function getClientRetryTimes();

    /**
     * 获取服务端异常引起的重试次数
     *
     * @return int
     */
    public function getServerRetryTimes();

    /**
     * 获取响应数据 Body 的数据流实例
     *
     * @param string $key       数据键名
     * @param mixed  $default   找不到数据时的默认值
     * @return mixed
     */
    public function getBody($key, $default = null);

    /**
     * 获取响应头数据数组
     *
     * @return array
     */
    public function getHeaders();

    /**
     * 判断返回的数据内容是否为 JSON 格式
     *
     * @return bool
     */
    public function isJson();

    /**
     * 将响应的数据解析成字符串
     *
     * @return string
     */
    public function toString();

    /**
     * 将返回数据解析成数组格式
     *
     * @return array
     */
    public function toArray();
}
