<?php

namespace Mingyuanyun\Core;

use Mingyuanyun\Core\Config\Config;
use Mingyuanyun\Core\Credentials\ClientCredentials;
use Mingyuanyun\Core\Credentials\Credentials;
use Mingyuanyun\Core\Exception\ConfigException;

/**
 * 明源云 SDK 全局类
 */
class SDK
{
    /** 版本 */
    const VERSION = 'v1.0.2';

    /** SDK 发起请求的统一请求头前缀 */
    const HEADER_PREFIX = 'x-opensdk-';

    /**
     * SDK 配置
     *
     * @var Config
     */
    public static $config;

    /**
     * 全局默认凭证类
     *
     * @var Credentials
     */
    public static $credentials;

    /**
     * 获取全局配置数据
     *
     * @return Config
     * @throws ConfigException
     */
    public static function config()
    {
        if (!static::$config) {
            throw new ConfigException('SDK no load config, please invoke loadConfig method and provide config data.');
        }
        return static::$config;
    }

    /**
     * 是否存在可用的全局配置
     *
     * @return bool
     */
    public static function hasConfig()
    {
        return !is_null(static::$config);
    }

    /**
     * 加载 SDK 全局配置数据
     *
     * @param array $configs 配置数据数组
     */
    public static function loadConfig(array $configs)
    {
        if (static::$config && static::$config instanceof Config) {
            return;
        }

        static::$config = Config::load($configs);
    }

    /**
     * 清除已经加载的 SDK 全局配置数据
     */
    public static function clearConfig()
    {
        static::$config = null;
    }

    /**
     * 获取发送器单例实例
     *
     * @return Sender
     *
     * @codeCoverageIgnore
     */
    public static function getSenderInstance()
    {
        // 当前默认返回 Http 实现的请求发送器
        if (HttpSender::hasMock()) {
            return new HttpSender;
        }
        return HttpSender::instance();
    }

    /**
     * 根据配置获取默认的凭证类
     *
     * @return ClientCredentials
     */
    public static function getDefaultCredentials()
    {
        if (static::$credentials && static::$credentials instanceof ClientCredentials) {
            return static::$credentials;
        }

        static::$credentials = Credentials::client(static::config()->secretId, static::config()->secretKey);
        return static::$credentials;
    }
}
