<?php

namespace Mingyuanyun\Core;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions as Options;
use Mingyuanyun\Core\Config\ServerConfig;
use Mingyuanyun\Core\Exception\ClientException;
use Mingyuanyun\Core\Exception\MYYSDKException;
use Mingyuanyun\Core\Support\Helper\ArrayHelper;
use Mingyuanyun\Core\Support\Helper\StringHelper;
use Mingyuanyun\Core\Support\Traits\MockTrait;
use Mingyuanyun\Core\Support\Traits\RetryTrait;
use Mingyuanyun\Core\Support\Traits\SingletonTrait;
use Mingyuanyun\Core\Validator\SenderValidator;

/**
 * 请求发送器
 *
 * @method static $this instance()
 */
abstract class Sender
{
    use SingletonTrait;
    use MockTrait;
    use RetryTrait;

    /**
     * 请求数据实例
     *
     * @var RequestInterface
     */
    protected $request;

    /**
     * 发起请求的方式
     *
     * @var string
     */
    private $method;

    /**
     * 请求路径/地址
     *
     * @var string
     */
    private $uri;

    /**
     * 通过 GuzzleHttp 发起请求所需的 options 数据
     *
     * @var array
     */
    private $options;

    /**
     * 请求处理客户端
     *
     * @var GuzzleHttpClient
     */
    private $client;

    /**
     * 构造器
     */
    public function __construct()
    {
        $this->options[Options::HTTP_ERRORS]     = false;
        $this->options[Options::CONNECT_TIMEOUT] = ServerConfig::DEFAULT_CONNECT_TIMEOUT;
        $this->options[Options::TIMEOUT]         = ServerConfig::DEFAULT_TIMEOUT;
        $this->options[Options::VERIFY]          = ServerConfig::DEFAULT_VERIFY;

        if ($this->hasMock()) {
            $this->options['handler'] = HandlerStack::create($this->getMock());
        }
        if (SDK::hasConfig() && SDK::config()->debug) {
            $this->options[Options::DEBUG] = SDK::config()->debug;
        }

        $this->client = new GuzzleHttpClient($this->options);
    }

    /**
     * Mock 一个发送器实例
     *
     * @return $this
     */
    public static function mock()
    {
        if (!static::hasMock()) {
            throw new MYYSDKException('Mock sender must be provide response or exception.');
        }
        return new static();
    }

    /**
     * 发起请求
     *
     * @param RequestInterface $request 请求数据
     * @return ResponseInterface
     */
    public function send(RequestInterface $request)
    {
        SenderValidator::request($request);

        $request->sending();
        $this->request = $request;

        $response = $this->sendRequest();

        if ($this->shouldRetryByServer($response)) {

            $this->serverRetry($request, $response);

            // 请求重试需要将请求数据类的状态恢复，并更新时间信息
            $request->reset();
            $request->setHeaders([
                StringHelper::hk('request-datetime')  => StringHelper::dateTime(),
                StringHelper::hk('request-timestamp') => time(),
            ])->pending();

            return $this->send($request);
        }

        $response = $this->appendRetryInfoToResponse($response);

        // TODO 记录请求日志

        $this->clearRequest();
        return $response;
    }

    /**
     * 设置发送请求所需要的 options 数据
     *
     * @param array $options
     * @return $this
     *
     * @see https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     */
    protected function setOptions($options)
    {
        if (empty($this->options)) {
            $this->options = $options;
        } else {
            $this->options = ArrayHelper::merge($this->options, $options);
        }
        return $this;
    }

    /**
     * 获取 Options 数据
     *
     * @return array
     *
     * @see https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * 处理发起请求所需的数据
     */
    private function resolveRequest()
    {
        if (!StringHelper::isUrl($this->request->getUri())) {
            $this->options['base_uri'] = SDK::config()->server->host;
        }

        // 超时时间设置，可根据请求维度单独设置（请求中设置的值优先级大于发送器默认值）
        if (!empty($this->request->getConnectTimeout())) {
            $this->options[Options::CONNECT_TIMEOUT] = $this->request->getConnectTimeout();
        }
        if (!empty($this->request->getTimeout())) {
            $this->options[Options::TIMEOUT] = $this->request->getTimeout();
        }

        if ($this->getClientRetryTimes($this->request) > 0) {
            $this->options[Options::HEADERS][StringHelper::hk('client-retry-times')] = $this->getClientRetryTimes($this->request);
        }
        if ($this->getServerRetryTimes($this->request) > 0) {
            $this->options[Options::HEADERS][StringHelper::hk('server-retry-times')] = $this->getServerRetryTimes($this->request);
        }

        $this->resolveUri();
        $this->resolveMethod();
        $this->resolveOptions();
    }

    /**
     * 发起同步请求
     *
     * @return Response
     * @throws ClientException
     */
    protected function sendRequest()
    {
        try {

            $this->resolveRequest();

            $gr = $this->client->request($this->method, $this->uri, $this->options);

            $this->request->complete();

            return new Response($this->request, $gr);

        } catch (TransferException $e) {

            if ($this->shouldRetryByClient($e)) {
                $this->clientRetry($this->request, $e);
                return $this->sendRequest();
            }

            $this->clearRequest();

            throw new ClientException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * 往响应数据类追加关联的请求重试信息
     *
     * @param ResponseInterface $response
     */
    private function appendRetryInfoToResponse(ResponseInterface $response)
    {
        $retrySnapshots = $this->generateRetrySnapshotStrings($this->request->getId());
        $response->setRetryInfo($retrySnapshots, $this->getClientRetryTimes($this->request), $this->getServerRetryTimes($this->request));
        return $response;
    }

    /**
     * 处理请求地址
     */
    private function resolveUri()
    {
        $this->uri = $this->request->getUri();
    }

    /**
     * 处理请求的方式
     */
    private function resolveMethod()
    {
        $this->method = $this->request->getMethod();
    }

    /**
     * 解决发起请求所需的 options 数据，从 request 中解析相关数据
     */
    abstract protected function resolveOptions();

    /**
     * 清除发送器中正在处理的请求信息
     */
    private function clearRequest()
    {
        $this->request = null;
        $this->uri     = null;
        $this->method  = null;
        $this->options = null;
    }
}
