<?php

namespace Mingyuanyun\Core\Support;

use Exception;
use Mingyuanyun\Core\RequestInterface;
use Mingyuanyun\Core\Support\Helper\StringHelper;

/**
 * 客户端重试记录
 */
class ClientRetryRecord
{
    /**
     * 重试时间，格式：YYYY-MM-DD HH:ii:ss
     *
     * @var string
     */
    public $datetime;

    /**
     * @var RequestInterface
     */
    public $request;

    /**
     * @var Exception
     */
    public $exception;

    /**
     * 客户端重试记录构造器
     *
     * @param RequestInterface  $request    请求数据
     * @param Exception         $e          抛出的异常
     * @param string            $datetime   重试时间
     */
    public function __construct(RequestInterface $request, Exception $e, $datetime = null)
    {
        $this->request   = $request;
        $this->exception = $e;

        if (!$datetime) {
            $this->datetime = StringHelper::dateTime();
        } else {
            $this->datetime = $datetime;
        }
    }
}
