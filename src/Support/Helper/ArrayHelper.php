<?php

namespace Mingyuanyun\Core\Support\Helper;

/**
 * 数组助手类
 */
class ArrayHelper
{
    /**
     * 从给定的数组中获取对应键的数据，支持 key1.key2 格式的深度遍历
     *
     * @param array  $array     数据源数组
     * @param string $key       数据键
     * @param mixed  $default   找不到数据时的默认返回值
     * @return mixed
     */
    public static function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }

        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }

        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            return $array[$key];
        }

        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key   = substr($key, $pos + 1);
        }

        if (is_object($array)) {
            // this is expected to fail if the property does not exist, or __get() is not implemented
            // it is not reliably possible to check whether a property is accessible beforehand
            return $array->$key;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        }

        return $default;
    }

    /**
     * 往给定的数组中填充设置键路径的数据，支持 key1.key2 格式的深度遍历
     *
     * @param array  $array 数据源数组
     * @param string $key   数据键路径
     * @param mixed  $value 数据值
     */
    public static function setValue(&$array, $path, $value)
    {
        if ($path === null) {
            $array = $value;
            return;
        }

        $keys = is_array($path) ? $path : explode('.', $path);

        while (count($keys) > 1) {
            $key = array_shift($keys);
            if (!isset($array[$key])) {
                $array[$key] = [];
            }
            if (!is_array($array[$key])) {
                $array[$key] = [$array[$key]];
            }
            $array = &$array[$key];
        }

        $array[array_shift($keys)] = $value;
    }

    /**
     * 将 $arrays 中的多个数组数据合并到 $res
     * 
     * @param array $res 主数组
     * @param array $arrays 多个待合并的数组
     * @return array
     */
    public static function merge(array $res, ...$arrays)
    {
        foreach ($arrays as $array) {
            foreach ($array as $key => $value) {
                if (is_int($key)) {
                    $res[] = $value;
                    continue;
                }

                if (isset($res[$key]) && is_array($res[$key])) {
                    $res[$key] = static::merge($res[$key], $value);
                    continue;
                }

                $res[$key] = $value;
            }
        }

        return $res;
    }
}
