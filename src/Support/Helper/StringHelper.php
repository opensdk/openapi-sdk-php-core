<?php

namespace Mingyuanyun\Core\Support\Helper;

use DateTime;
use Mingyuanyun\Core\SDK;

class StringHelper
{
    /**
     * 判断给定字符串中是否出现过特定的内容
     * 在草垛 $haystack 种判断是否有针 $needle
     *
     * @param string $haystack  完整字符串
     * @param string $needle    匹配的字符串内容
     */
    public static function contains($haystack, $needle)
    {
        return false !== strpos((string) $haystack, (string) $needle);
    }

    /**
     * 生成一段随机 ID 值
     *
     * @param string $prefix 随机值前缀
     * @return string
     */
    public static function generateId($prefix = '')
    {
        return str_replace('.', '', uniqid($prefix, true));
    }

    /**
     * 根据 Header Key 规范返回对应的 Key 值
     *
     * @param string $s Key 值
     * @return string
     */
    public static function headerKey($s)
    {
        if (empty($s)) {
            return '';
        }
        if (substr($s, 0, strlen(SDK::HEADER_PREFIX)) === SDK::HEADER_PREFIX) {
            return $s;
        }
        return SDK::HEADER_PREFIX . $s;
    }

    /**
     * headerKey 方法的别名
     *
     * @param string $s Key 值
     * @return string
     */
    public static function hk($s)
    {
        return self::headerKey($s);
    }

    /**
     * 返回格式化的当前时间，单位：微秒
     * 示例：2023-01-06 10:58:20.245122
     *
     * @return string
     */
    public static function dateTime()
    {
        // 只有当 PHP 版本大于 7.1 时，DateTime 才支持输出包含毫秒的格式化内容
        // 因此小于 7.1 的 PHP 版本下，需要自行拼接毫秒数据
        if (version_compare(PHP_VERSION, '7.1', '>=')) {
            $dateTime = (new DateTime())->format('Y-m-d H:i:s.u');
        } else {
            list($d, $u) = explode('.', microtime(true));
            $dateTime    = date('Y-m-d H:i:s.', $d) . $u;
        }
        return $dateTime;
    }

    /**
     * 判断一段字符串是否为接口地址
     *
     * @param string $url
     * @return bool
     */
    public static function isUrl($url)
    {
        if (empty($url) || !is_string($url) || !preg_match('/^http|https.*?/', $url)) {
            return false;
        }
        return true;
    }
}
