<?php

namespace Mingyuanyun\Core\Support;

use Mingyuanyun\Core\Support\Traits\AccessTrait;

/**
 * 请求选项类
 */
class RequestOptions
{
    use AccessTrait;

    /** 请求路径 */
    const URI = 'uri';

    /** 请求方式 */
    const METHOD = 'method';

    /** 请求头 */
    const HEADER = 'header';

    /** 请求查询 */
    const QUERY = 'query';

    /** 请求体 */
    const BODY = 'body';

    /** 授权凭证 */
    const AUTHORIZATION = 'authorization';

    /** 连接超时 */
    const CONNECT_TIMEOUT = 'connectTimeout';

    /** 响应超时 */
    const TIMEOUT = 'timeout';

    /** 请求体数据编码格式 */
    const FORMAT = 'format';

    /** 是否校验证书 */
    const VERIFY = 'verify';

    /** 请求方式：GET 请求 */
    const METHOD_GET = 'GET';

    /** 请求方式：POST 请求 */
    const METHOD_POST = 'POST';

    /** 请求方式：PUT 请求 */
    const METHOD_PUT = 'PUT';

    /** 请求方式：DELETE 请求 */
    const METHOD_DELETE = 'DELETE';

    /** 数据编码格式：Content-Type = application/json */
    const FORMAT_JSON = 'JSON';

    /** 数据编码格式：Content-Type = application/x-www-form-urlencoded */
    const FORMAT_FORM = 'FORM';
}
