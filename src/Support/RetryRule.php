<?php

namespace Mingyuanyun\Core\Support;

use Mingyuanyun\Core\Exception\ClientException;
use Mingyuanyun\Core\Response;
use Mingyuanyun\Core\Support\Traits\AccessTrait;
use Mingyuanyun\Core\Validator\RetryValidator;

/**
 * 请求重试规则
 * 
 * @method int      getClientRetryTimes()   return field $clientRetryTimes value.
 * @method int[]    getClientRetryCodes()   return field $clientRetryCodes value.
 * @method string[] getClientRetryStrings() return field $clientRetryCodes value.
 * @method int      getServerRetryTimes()   return field $serverRetryTimes value.
 * @method int[]    getServerRetryCodes()   return field $serverRetryCodes value.
 * @method string[] getServerRetryStrings() return field $serverRetryCodes value.
 */
class RetryRule
{
    use AccessTrait;
    
    /**
     * 客户端问题最大重试次数
     *
     * @var int
     */
    private $clientRetryTimes = 1;

    /**
     * 客户端问题需要触发重试的错误信息数组
     *
     * @var int[]
     */
    private $clientRetryCodes = [];

    /**
     * 客户端问题需要触发重试的错误信息数组
     *
     * @var string[]
     */
    private $clientRetryStrings = [];

    /**
     * 服务端问题最大重试次数
     *
     * @var int
     */
    private $serverRetryTimes = 0;

    /**
     * 服务端问题需要触发重试的错误信息数组
     *
     * @var int[]
     */
    private $serverRetryCodes = [];

    /**
     * 服务端问题需要触发重试的错误信息数组
     *
     * @var string[]
     */
    private $serverRetryStrings = [];

    /**
     * 设置与客户端有关的重试规则
     *
     * @param int   $times      重试次数
     * @param array $strings    触发重试的客户端异常信息内容数组
     * @param array $codes      触发重试的客户端异常状态码
     * @return $this
     */
    public function retryByClient($times, array $strings, array $codes = [])
    {
        RetryValidator::times($times);
        RetryValidator::strings($strings);
        RetryValidator::codes($codes);

        $this->clientRetryTimes   = $times;
        $this->clientRetryStrings = $strings;
        $this->clientRetryCodes   = $codes;

        return $this;
    }

    /**
     * 设置与服务端有关的重试规则
     *
     * @param int   $times      重试次数
     * @param array $strings    触发重试的服务端异常信息内容数组
     * @param array $codes      触发重试的服务端异常状态码
     * @return $this
     */
    public function retryByServer($times, array $strings, array $codes = [])
    {
        RetryValidator::times($times);
        RetryValidator::strings($strings);
        RetryValidator::codes($codes);

        $this->serverRetryTimes   = $times;
        $this->serverRetryStrings = $strings;
        $this->serverRetryCodes   = $codes;

        return $this;
    }
}
