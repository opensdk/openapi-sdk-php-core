<?php

namespace Mingyuanyun\Core\Support;

use Mingyuanyun\Core\RequestInterface;
use Mingyuanyun\Core\ResponseInterface;
use Mingyuanyun\Core\Support\Helper\StringHelper;

/**
 * 服务端重试记录
 */
class ServerRetryRecord
{
    /**
     * 重试时间，格式：YYYY-MM-DD HH:ii:ss
     *
     * @var string
     */
    public $datetime;

    /**
     * @var RequestInterface
     */
    public $request;

    /**
     * @var ResponseInterface
     */
    public $response;

    /**
     * 客户端重试记录构造器
     *
     * @param RequestInterface  $request    请求数据
     * @param ResponseInterface $response   响应数据
     * @param string            $datetime   重试时间
     */
    public function __construct(RequestInterface $request, ResponseInterface $response, $datetime = null)
    {
        $this->request  = $request;
        $this->response = $response;

        if (!$datetime) {
            $this->datetime = StringHelper::dateTime();
        } else {
            $this->datetime = $datetime;
        }
    }
}
