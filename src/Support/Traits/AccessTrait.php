<?php

namespace Mingyuanyun\Core\Support\Traits;

use Mingyuanyun\Core\Exception\MYYSDKException;

/**
 * 类数据访问特性
 */
trait AccessTrait
{
    /**
     * 通过魔术方法访问类私有属性
     */
    public function __call($method, $args)
    {
        if (strncmp($method, 'get', 3) === 0) {
            $fieldName = lcfirst(mb_strcut($method, 3));
            return $this->{$fieldName};
        }

        throw new MYYSDKException("Call to undefined method __CLASS__::{$method}");
    }
}
