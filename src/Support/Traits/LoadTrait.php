<?php

namespace Mingyuanyun\Core\Support\Traits;

trait LoadTrait
{
    public function load(array $values)
    {
        foreach ($values as $name => $value) {
            if (!is_null($value) && property_exists($this, $name)) {
                $this->{$name} = $value;
            }
        }
    }
}