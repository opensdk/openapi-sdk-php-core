<?php

namespace Mingyuanyun\Core\Support\Traits;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Mock 请求特性类
 */
trait MockTrait
{
    /**
     * @var array
     */
    private static $mockQueue = [];

    /**
     * @var MockHandler
     */
    private static $mock;

    /**
     * Mock 一个正常返回的 Response
     *
     * @param integer             $status
     * @param array               $headers
     * @param array|string|object $body
     */
    public static function mockResponse($status = 200, array $headers = [], $body = null)
    {
        if (is_array($body) || is_object($body)) {
            $body = json_encode($body);
        }

        self::$mockQueue[] = new Response($status, $headers, $body);
    }

    /**
     * Mock 一个会抛出异常的请求
     *
     * @param string                 $message
     * @param RequestInterface       $request
     * @param ResponseInterface|null $response
     */
    public static function mockRequestException(
        $message,
        RequestInterface $request,
        ResponseInterface $response = null
    ) {
        self::$mockQueue[] = new RequestException(
            $message,
            $request,
            $response
        );
    }

    /**
     * @return void
     */
    public static function cancelMock()
    {
        self::$mockQueue = [];
        self::$mock      = null;
    }

    /**
     * @return bool
     */
    public static function hasMock()
    {
        return !empty(self::$mockQueue);
    }

    /**
     * @return MockHandler
     */
    public static function getMock()
    {
        if (!self::$mock) {
            self::$mock = new MockHandler(self::$mockQueue);
        }
        return self::$mock;
    }
}
