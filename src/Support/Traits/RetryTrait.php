<?php

namespace Mingyuanyun\Core\Support\Traits;

use Exception;
use Mingyuanyun\Core\Request;
use Mingyuanyun\Core\RequestInterface;
use Mingyuanyun\Core\ResponseInterface;
use Mingyuanyun\Core\Support\ClientRetryRecord;
use Mingyuanyun\Core\Support\Helper\StringHelper;
use Mingyuanyun\Core\Support\ServerRetryRecord;

trait RetryTrait
{
    /**
     * 客户端请求重试数据池
     * 数据格式：
     * ```php
     * ['request-id' => [
     *   '2023-01-28 14:21:03' => ClientRetryRecord
     * ]]
     * ```
     *
     * @var array
     */
    private $clientRetryPool;

    /**
     * 服务端请求重试数据池
     * 数据格式：
     * ```php
     * ['request-id' => [
     *   '2023-01-28 14:21:03' => ServerRetryRecord
     * ]]
     * ```
     *
     * @var array
     */
    private $serverRetryPool;

    /**
     * 获取对应请求的客户端重试次数
     *
     * @param RequestInterface $request
     * @return int
     */
    protected function getClientRetryTimes(RequestInterface $request)
    {
        if (!isset($this->clientRetryPool[$request->getId()])) {
            return 0;
        }
        return count($this->clientRetryPool[$request->getId()]);
    }

    /**
     * 获取对应请求的服务端重试次数
     *
     * @param RequestInterface $request
     * @return int
     */
    protected function getServerRetryTimes(RequestInterface $request)
    {
        if (!isset($this->serverRetryPool[$request->getId()])) {
            return 0;
        }
        return count($this->serverRetryPool[$request->getId()]);
    }

    /**
     * 判断当前处理的请求是否需要发起客户端重试
     *
     * @param Exception $e
     * @return bool
     */
    protected function shouldRetryByClient(Exception $e)
    {
        if ($this->getClientRetryTimes($this->request) >= $this->request->getRetryRule()->getClientRetryTimes()) {
            return false;
        }

        if (in_array($e->getCode(), $this->request->getRetryRule()->getClientRetryCodes())) {
            return true;
        }

        foreach ($this->request->getRetryRule()->getClientRetryStrings() as $s) {
            if (StringHelper::contains($e->getMessage(), $s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断当前处理的请求是否需要发起服务端重试
     *
     * @param ResponseInterface $response
     * @return bool
     */
    protected function shouldRetryByServer(ResponseInterface $response)
    {
        if ($this->getServerRetryTimes($this->request) >= $this->request->getRetryRule()->getServerRetryTimes()) {
            return false;
        }

        if (in_array($response->getStatusCode(), $this->request->getRetryRule()->getServerRetryCodes())) {
            return true;
        }

        // 服务端异常时，取 Body 的错误信息；业务错误时，取返回数据中 message 字段的内容
        $message = $response->toString();
        foreach ($this->request->getRetryRule()->getServerRetryStrings() as $s) {
            if (StringHelper::contains($message, $s)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 记录一次客户端重试
     *
     * @param RequestInterface $request
     * @param Exception $e
     */
    protected function clientRetry(RequestInterface $request, Exception $e)
    {
        $retryRecord = new ClientRetryRecord($request, $e);

        $this->clientRetryPool[$request->getId()][$retryRecord->datetime] = $retryRecord;
    }

    /**
     * 记录一次服务端重试
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     */
    protected function serverRetry(RequestInterface $request, ResponseInterface $response)
    {
        $retryRecord = new ServerRetryRecord($request, $response);

        $this->serverRetryPool[$request->getId()][$retryRecord->datetime] = $retryRecord;
    }

    /**
     * 生成指定请求 ID 生成对应的重试记录快照清单。该函数会把所有客户端、服务端的
     * 重试记录按时间排序生成对应的字符串格式内容清单
     *
     * @param string $requestId 请求唯一 ID
     * @return string[]
     */
    protected function generateRetrySnapshotStrings($requestId)
    {
        if (empty($requestId)) {
            return [];
        }

        /** @var ClientRetryRecord[] */
        $clientRetryRecords = $this->clientRetryPool[$requestId] ?? [];
        /** @var ServerRetryRecord[] */
        $serverRetryRecords = $this->serverRetryPool[$requestId] ?? [];

        $snapshots = [];

        foreach ($clientRetryRecords as $cr) {
            $snapshots[$cr->datetime] = ['type' => 'clientRetry', 'request' => $cr->request->toString(), 'exception' => $cr->exception->getMessage()];
        }
        foreach ($serverRetryRecords as $sr) {
            $snapshots[$sr->datetime] = ['type' => 'serverRetry', 'request' => $sr->request->toString(), 'response' => $sr->response->toString()];
        }

        ksort($snapshots);
        return $snapshots;
    }
}
