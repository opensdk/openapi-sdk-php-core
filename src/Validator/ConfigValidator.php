<?php

namespace Mingyuanyun\Core\Validator;

use Mingyuanyun\Core\Exception\InvalidArgumentException;

/**
 * 配置项校验器
 */
class ConfigValidator
{
    /**
     * 密钥 Id 配置校验
     * CredentialsValidator::secretId 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function secretId($secretId)
    {
        CredentialsValidator::secretId($secretId);
    }

    /**
     * 密钥 Key 配置校验
     * CredentialsValidator::secretKey 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function secretKey($secretKey)
    {
        CredentialsValidator::secretKey($secretKey);
    }

    /**
     * Debug 模式配置校验
     *
     * @throws InvalidArgumentException
     */
    public static function debug($debug)
    {
        if (!is_bool($debug)) {
            throw new InvalidArgumentException('$debug must be a boolean.');
        }
    }

    /**
     * 开放服务地址校验
     * RequestValidator::host 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function serverHost($host)
    {
        RequestValidator::host($host);
    }

    /**
     * 开放服务地址校验
     * RequestValidator::host 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function serverConnectTimeout($sec)
    {
        RequestValidator::connectTimeout($sec);
    }

    /**
     * 开放服务地址校验
     * RequestValidator::host 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function serverTimeout($sec)
    {
        RequestValidator::timeout($sec);
    }

    /**
     * 开放服务地址校验
     * RequestValidator::host 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function serverVerify($verify)
    {
        RequestValidator::verify($verify);
    }

    /**
     * 通用代理地址校验
     * RequestValidator::proxy 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function proxy($proxy)
    {
        RequestValidator::proxy($proxy);
    }

    /**
     * http 请求所使用的代理地址校验
     * RequestValidator::proxy 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function httpProxy($proxy)
    {
        RequestValidator::proxy($proxy);
    }

    /**
     * https 请求所使用的代理地址校验
     * RequestValidator::proxy 方法的别名
     *
     * @throws InvalidArgumentException
     */
    public static function httpsProxy($proxy)
    {
        RequestValidator::proxy($proxy);
    }

    /**
     * 代理白名单地址校验
     *
     * @throws InvalidArgumentException
     */
    public static function proxyWhiteList($whiteList)
    {

    }
}
