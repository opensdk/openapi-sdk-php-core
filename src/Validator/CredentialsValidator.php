<?php

namespace Mingyuanyun\Core\Validator;

use Mingyuanyun\Core\Exception\InvalidArgumentException;

/**
 * 访问凭证校验器
 */
class CredentialsValidator
{
    /**
     * 校验凭证最基础的密钥数据是否有效
     *
     * @throws InvalidArgumentException
     */
    public static function secret($secretId, $secretKey)
    {
        static::secretId($secretId);
        static::secretKey($secretKey);
    }

    /**
     * 校验密钥 ID 是否有效
     *
     * @throws InvalidArgumentException
     */
    public static function secretId($secretId)
    {
        if (empty($secretId)) {
            throw new InvalidArgumentException('$secretId cannot be empty.');
        }
        if (!is_string($secretId)) {
            throw new InvalidArgumentException('$secretId must be a string.');
        }
    }

    /**
     * 校验密钥 Key 是否有效
     *
     * @throws InvalidArgumentException
     */
    public static function secretKey($secretKey)
    {
        if (empty($secretKey)) {
            throw new InvalidArgumentException('$secretKey cannot be empty.');
        }
        if (!is_string($secretKey)) {
            throw new InvalidArgumentException('$secretKey must be a string.');
        }
    }

    /**
     * 校验 OAuth2.0 client_credentials 鉴权模式下所需的 Token 是否有效
     *
     * @throws InvalidArgumentException
     */
    public static function token($token)
    {
        if (!is_string($token)) {
            throw new InvalidArgumentException('$token must be a string.');
        }
    }
}
