<?php

namespace Mingyuanyun\Core\Validator;

use Mingyuanyun\Core\Exception\InvalidArgumentException;
use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\Request;

/**
 * 请求数据校验器
 */
class RequestValidator
{
    /**
     * @param mixed $url
     * @throws InvalidArgumentException
     */
    public static function url($url)
    {
        if (empty($url)) {
            throw new InvalidArgumentException('$url cannot be empty.');
        }
        if (!is_string($url)) {
            throw new InvalidArgumentException('$url must be a string.');
        }
    }

    /**
     * @param mixed $host
     * @throws InvalidArgumentException
     */
    public static function host($host)
    {
        if (empty($host)) {
            throw new InvalidArgumentException('$host cannot be empty.');
        }
        if (!is_string($host)) {
            throw new InvalidArgumentException('$host must be a string.');
        }
        if (!preg_match('/^http|https.*?/', $host)) {
            throw new InvalidArgumentException('$host must begin with http|https.');
        }
    }

    /**
     * @param mixed $port
     * @throws InvalidArgumentException
     */
    public static function port($port)
    {
        if (empty($port)) {
            throw new InvalidArgumentException('$port cannot be empty.');
        }
        if (!is_numeric($port)) {
            throw new InvalidArgumentException('$port must be a number.');
        }
        if ($port < 0) {
            throw new InvalidArgumentException('$port must be > 0.');
        }
    }

    /**
     * @param mixed $uri
     * @throws InvalidArgumentException
     */
    public static function uri($uri)
    {
        if (empty($uri)) {
            throw new InvalidArgumentException('$uri cannot be empty.');
        }
        if (!is_string($uri)) {
            throw new InvalidArgumentException('$uri must be a string.');
        }
    }

    /**
     * @param mixed $method
     * @throws InvalidArgumentException
     */
    public static function method($method)
    {
        if (empty($method)) {
            throw new InvalidArgumentException('$method cannot be empty.');
        }
        if (!is_string($method)) {
            throw new InvalidArgumentException('$method must be a string.');
        }
        if (!in_array($method, ['GET', 'POST', 'PUT', 'DELETE'])) {
            throw new InvalidArgumentException("\$method need GET|POST|PUT|DELETE, but {$method} given.");
        }
    }

    /**
     * @param mixed $query
     * @throws InvalidArgumentException
     */
    public static function query($query)
    {
        if (!is_array($query)) {
            throw new InvalidArgumentException('$query must be a array.');
        }
    }

    /**
     * @param mixed $headers
     * @throws InvalidArgumentException
     */
    public static function headers($headers)
    {
        if (!is_array($headers)) {
            throw new InvalidArgumentException('$headers must be a array.');
        }
    }

    /**
     * @param mixed $body
     * @throws InvalidArgumentException
     */
    public static function body($body)
    {
        if (!is_array($body)) {
            throw new InvalidArgumentException('$body must be a array.');
        }
    }

    /**
     * @param mixed $cookies
     * @throws InvalidArgumentException
     */
    public static function cookies($cookies)
    {
        if (!is_array($cookies)) {
            throw new InvalidArgumentException('$cookies must be a array.');
        }
    }

    /**
     * @param mixed $authorization
     * @throws InvalidArgumentException
     */
    public static function authorization($authorization)
    {
        if (empty($authorization)) {
            throw new InvalidArgumentException('$authorization cannot be empty.');
        }
        if (!is_string($authorization)) {
            throw new InvalidArgumentException('$authorization must be a string.');
        }
    }

    /**
     * @param mixed $format
     * @throws InvalidArgumentException
     */
    public static function format($format)
    {
        if (empty($format)) {
            throw new InvalidArgumentException('$format cannot be empty.');
        }
        if (!is_string($format)) {
            throw new InvalidArgumentException('$format must be a string.');
        }
        if (!in_array($format, ['JSON', 'FORM'])) {
            throw new InvalidArgumentException("\$format need JSON|FORM, but {$format} given.");
        }
    }

    /**
     * @param mixed $sec
     * @throws InvalidArgumentException
     */
    public static function connectTimeout($sec)
    {
        if (empty($sec)) {
            throw new InvalidArgumentException('$sec cannot be empty.');
        }
        if (!is_float($sec)) {
            throw new InvalidArgumentException('$sec must be a float.');
        }
    }

    /**
     * @param mixed $sec
     * @throws InvalidArgumentException
     */
    public static function timeout($sec)
    {
        if (empty($sec)) {
            throw new InvalidArgumentException('$sec cannot be empty.');
        }
        if (!is_float($sec)) {
            throw new InvalidArgumentException('$sec must be a float.');
        }
    }

    /**
     * @param mixed $proxy
     * @throws InvalidArgumentException
     */
    public static function proxy($proxy)
    {
        if (empty($proxy)) {
            throw new InvalidArgumentException('$proxy cannot be empty.');
        }
        if (!is_string($proxy)) {
            throw new InvalidArgumentException('$proxy must be a string.');
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public static function verify($verify)
    {
        if (!is_bool($verify)) {
            throw new InvalidArgumentException('$verify must be a boolean.');
        }
    }

    /**
     * 更新请求数据校验
     *
     * @throws RequestException
     */
    public static function changeProps(Request $request)
    {
        if ($request->getStatus() !== Request::INIT) {
            throw new RequestException('Change request props must be in the init status.');
        }
    }

    /**
     * 更新重试信息校验
     *
     * @throws RequestException
     */
    public static function retry(Request $request)
    {
        if ($request->getStatus() !== Request::SENDING) {
            throw new RequestException('Update request retry info must be in the sending status.');
        }
    }
}
