<?php

namespace Mingyuanyun\Core\Validator;

use Mingyuanyun\Core\Exception\InvalidArgumentException;

/**
 * 重试规则校验器
 */
class RetryValidator
{
    /**
     * @param mixed $times
     * @throws InvalidArgumentException
     */
    public static function times($times)
    {
        if (!is_int($times)) {
            throw new InvalidArgumentException('$times must be int.');
        }
        if ($times > 3) {
            throw new InvalidArgumentException('Max retry times limit is 3.');
        }
    }

    /**
     * @param array $codes
     * @throws InvalidArgumentException
     */
    public static function codes(array $codes)
    {
        foreach ($codes as $i => $c) {
            if (!is_int($c)) {
                throw new InvalidArgumentException("\$codes[{$i}] must be int.");
            }
        }
    }

    /**
     * @param array $strings
     * @throws InvalidArgumentException
     */
    public static function strings(array $strings)
    {
        foreach ($strings as $i => $s) {
            if (!is_string($s)) {
                throw new InvalidArgumentException("\$strings[{$i}] must be a string.");
            }
        }
    }
}