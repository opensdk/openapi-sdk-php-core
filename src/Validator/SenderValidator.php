<?php

namespace Mingyuanyun\Core\Validator;

use InvalidArgumentException;
use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\Request;
use Mingyuanyun\Core\RequestInterface;

/**
 * 请求发送器的校验器
 */
class SenderValidator
{
    /**
     * 请求数据校验
     *
     * @param RequestInterface $request
     * @throws InvalidArgumentException
     */
    public static function request(RequestInterface $request)
    {
        if (empty($request->getUri())) {
            throw new RequestException('$uri cannot be empty.');
        }
        if ($request->getStatus() !== Request::PENDING) {
            throw new RequestException('Only request can be send that it\'s status at pending.');
        }
    }
}
