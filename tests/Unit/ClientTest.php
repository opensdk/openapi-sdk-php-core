<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\Client;
use Mingyuanyun\Core\HttpSender;
use Mingyuanyun\Core\Request;
use Mingyuanyun\Core\Support\RequestOptions;
use PHPUnit\Framework\TestCase;

/**
 * 客户端测试类
 */
class ClientTest extends TestCase
{
    protected function tearDown(): void
    {
        HttpSender::cancelMock();
    }

    // 通过开放服务客户端发起一个服务请求
    public function test_should_invoke_success()
    {
        HttpSender::mockResponse(200, [], ['errcode' => 0, 'message' => 'success']);

        $request = new Request(RequestOptions::METHOD_GET, 'https://www.server.com/get');

        $client = new Client($request);

        $response = $client->invoke();

        $this->assertNotNull($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('success', $response->getBody('message'));
    }
}
