<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\Config\Config;
use Mingyuanyun\Core\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    /**
     * @var array
     */
    protected $config;

    protected function setUp(): void
    {
        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'server' => [
                'host' => 'https://www.server.com',
            ],
        ];
    }

    // 正常情况下通过 SDK 应该可以成功将配置加载成全局数据
    public function test_should_load_global_config()
    {
        $config = Config::load($this->config);

        $this->assertEquals('mingyuanyun_secret_id_123', $config->secretId);
        $this->assertEquals('mingyuanyun_secret_key_666', $config->secretKey);
        $this->assertEquals('https://www.server.com', $config->server->host);
    }

    // 如果提供了无效的密钥配置，应该要抛出相关的异常信息
    public function test_should_throw_exception_if_got_invalid_secret()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/cannot be empty/');

        $this->config['secretId'] = '';
        $this->config['secretKey'] = '';

        Config::load($this->config);
    }

    // 如果提供了无效的服务地址配置，应该要抛出相关的异常信息
    public function test_should_throw_exception_if_got_invalid_host()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/cannot be empty/');

        $this->config['server']['host'] = '';

        Config::load($this->config);
    }

    // 如果提供了正确的代理地址，需要能成功加载对应配置数据
    public function test_should_load_proxy_config()
    {
        $this->config['proxy'] = [
            'proxy'     => 'tcp://127.0.0.1:9090',
            'httpProxy' => 'tcp://127.0.0.1:9091',
            'httpsProxy' => 'tcp://127.0.0.1:9092',
            'whiteList' => [
                'https://www.server.com/api/white-list',
            ],
        ];

        $config = Config::load($this->config);

        $this->assertEquals('tcp://127.0.0.1:9090', $config->proxy->proxy);
        $this->assertEquals('tcp://127.0.0.1:9091', $config->proxy->httpProxy);
        $this->assertEquals('tcp://127.0.0.1:9092', $config->proxy->httpsProxy);
        $this->assertIsArray($config->proxy->whiteList);
        $this->assertEquals(1, count($config->proxy->whiteList));
    }

    // 如果提供了无效的代理配置，应该要抛出相关的异常信息
    public function test_should_throw_exception_if_got_invalid_proxy()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/must be a string/');
        
        $this->config['proxy'] = [
            'proxy'     => 123,
            'httpProxy' => 123,
            'httpsProxy' => 123,
        ];

        Config::load($this->config);
    }
}