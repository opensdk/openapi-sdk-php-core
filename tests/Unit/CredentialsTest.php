<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\Credentials\ClientCredentials;
use Mingyuanyun\Core\Credentials\Credentials;
use Mingyuanyun\Core\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * 访问凭证测试类
 */
class CredentialsTest extends TestCase
{
    /**
     * @var array
     */
    protected $secret;

    /**
     * @var string
     */
    protected $token;

    protected function setUp(): void
    {
        $this->secret = [
            'secretId'  => 'mingyuanyun_secret_123',
            'secretKey' => 'mingyuanyun_secret_666',
        ];
        $this->token = 'mingyuanyun_token_123';
    }

    // 通过构造函数可以正常加载相应的密钥数据
    public function test_should_load_secret_success_with_constructor()
    {
        $credentials = Credentials::client($this->secret['secretId'], $this->secret['secretKey'], $this->token);

        $this->assertInstanceOf(ClientCredentials::class, $credentials);
        $this->assertEquals('mingyuanyun_secret_123', $credentials->getSecretId());
        $this->assertEquals('mingyuanyun_secret_666', $credentials->getSecretKey());
        $this->assertEquals('mingyuanyun_token_123', $credentials->getToken());
    }

    // 通过 make 函数可以正常加载相应的密钥数据
    public function test_should_load_secret_success_with_make()
    {
        $credentials = Credentials::client($this->secret['secretId'], $this->secret['secretKey'], $this->token);

        $this->assertInstanceOf(ClientCredentials::class, $credentials);
        $this->assertEquals('mingyuanyun_secret_123', $credentials->getSecretId());
        $this->assertEquals('mingyuanyun_secret_666', $credentials->getSecretKey());
        $this->assertEquals('mingyuanyun_token_123', $credentials->getToken());
    }

    // 如果提供了空的 secret 数据，应该要抛出对应的异常
    public function test_should_throw_exception_if_got_empty_secret()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/cannot be empty/');
        
        Credentials::client('', '');
    }

    // 如果提供了错误数据类型的 secret 数据，应该要抛出对应的异常
    public function test_shoould_throw_exception_if_got_secret_data_typeof_int()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/must be a string/');

        Credentials::client(123, 321);
    }

    // 如果只是提供了空的 Token 数据，应该要可以正常实例化凭证类
    public function test_should_load_secret_if_got_empty_token()
    {
        $credentials = Credentials::client($this->secret['secretId'], $this->secret['secretKey'], '');

        $this->assertInstanceOf(ClientCredentials::class, $credentials);
        $this->assertEquals('mingyuanyun_secret_123', $credentials->getSecretId());
        $this->assertEquals('mingyuanyun_secret_666', $credentials->getSecretKey());
        $this->assertEquals('', $credentials->getToken());
    }

    // 如果提供了类型错误的 Token 数据，应该要抛出对应的异常
    public function test_should_throw_exception_if_got_invalid_token()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessageMatches('/must be a string/');

        Credentials::client($this->secret['secretId'], $this->secret['secretKey'], 123);
    }
}
