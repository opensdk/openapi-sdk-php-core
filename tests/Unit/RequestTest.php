<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\Exception\RequestException;
use Mingyuanyun\Core\Request;
use Mingyuanyun\Core\Support\RequestOptions;
use Mingyuanyun\Core\Support\RetryRule;
use PHPUnit\Framework\TestCase;

/**
 * 请求测试类
 */
class RequestTest extends TestCase
{
    /**
     * @var Request
     */
    protected $request;
    
    protected function setUp(): void
    {
        $this->request = new Request(RequestOptions::METHOD_POST, '/api');
        $this->request->setQuery(['token' => '123'])
            ->setHeaders(['x-opensdk-tag' => 'openapi-sdk-php-core'])
            ->setBody(['id' => 1]);

        $retryRule = new RetryRule();
        $retryRule->retryByClient(1, ['client retry']);
        $retryRule->retryByServer(1, ['server retry']);
        $this->request->setRetryRule($retryRule)->pending();
    }
    
    // 通过链式调用 set 方法，应该可以正常给请求的属性设置上对应的数据
    public function test_should_set_props_by_stream_set_method()
    {
        $request = $this->request;
        $request->setFormat(RequestOptions::FORMAT_JSON);

        $this->assertEquals('/api', $request->getUri());
        $this->assertEquals(RequestOptions::METHOD_POST, $request->getMethod());
        $this->assertNotEmpty($request->getQuery());
        $this->assertNotEmpty($request->getHeaders());
        $this->assertNotEmpty($request->getBody());
        $this->assertEquals(RequestOptions::FORMAT_JSON, $request->getFormat());
    }
    
    // 处于发送中状态的请求，修改请求数据需要抛出异常
    public function test_should_throw_exception_if_change_props_in_the_sending()
    {
        $request = $this->request;
        $request->sending();

        $this->expectException(RequestException::class);

        $request->setQuery(['b' => '123']);
    }
    
    // 在请求完成状态下修改请求数据，应该要抛出异常
    public function test_should_throw_exception_if_change_props_in_the_complete()
    {
        $request = $this->request;
        $request->sending();
        $request->complete();

        $this->expectException(RequestException::class);

        $request->setQuery(['b' => '123']);
    }

    // 如果请求的状态直接从已就绪变更为已完成，应该要抛出异常
    public function test_should_throw_exception_if_status_change_to_complete_from_pending()
    {
        $this->expectException(RequestException::class);
        
        $this->request->complete();
    }

    // 如果请求的状态不是从已就绪变更为发送中，应该要抛出异常
    public function test_should_throw_exception_if_status_change_to_sending_no_from_pending()
    {
        $this->request->sending();
        $this->request->complete();

        $this->expectException(RequestException::class);

        $this->request->sending();
    }
}
