<?php

namespace Mingyuanyun\Tests\Unit;

use Mingyuanyun\Core\Config\Config;
use Mingyuanyun\Core\Exception\ConfigException;
use Mingyuanyun\Core\SDK;
use PHPUnit\Framework\TestCase;

/**
 * 全局 SDK 测试类
 */
class SDKTest extends TestCase
{
    /**
     * @var array
     */
    protected $config;

    protected function setUp(): void
    {
        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'server' => [
                'host' => 'https://www.server.com',
            ],
        ];

        SDK::loadConfig($this->config);
    }

    protected function tearDown(): void
    {
        SDK::clearConfig();
    }

    // 正常情况下通过 SDK 应该可以成功将配置加载成全局数据
    public function test_should_load_global_config()
    {
        $this->assertInstanceOf(Config::class, SDK::$config);
        $this->assertTrue(SDK::hasConfig());
        $this->assertEquals('mingyuanyun_secret_id_123', SDK::$config->secretId);
        $this->assertEquals('mingyuanyun_secret_key_666', SDK::$config->secretKey);
        $this->assertEquals('https://www.server.com', SDK::$config->server->host);
    }

    // 如果重复执行加载配置的动作，应该要加载新数据失败，SDK 中仍是旧数据
    public function test_should_load_faild_if_action_is_repeated()
    {
        $this->config['secretId'] = 'mingyuanyun_secret_id_321';
        SDK::loadConfig($this->config);

        $this->assertEquals('mingyuanyun_secret_id_123', SDK::$config->secretId);
    }

    // 如果没有加载配置数据，直接使用配置类，应该要抛出异常
    public function test_should_get_config_throw_exception_if_no_load_config()
    {
        SDK::clearConfig();

        $this->expectException(ConfigException::class);

        SDK::config();
    }

    // 根据配置获取全局默认的凭证数据
    public function test_should_get_default_credentials()
    {
        $credentials = SDK::getDefaultCredentials();

        $this->assertNotNull($credentials);
        $this->assertEquals('mingyuanyun_secret_id_123', $credentials->getSecretId());
        $this->assertEquals('mingyuanyun_secret_key_666', $credentials->getSecretKey());
    }
}