<?php

namespace Mingyuanyun\Tests\Unit;

use GuzzleHttp\Psr7\Request as Psr7Request;
use Mingyuanyun\Core\Exception\ClientException;
use Mingyuanyun\Core\HttpSender;
use Mingyuanyun\Core\Request;
use Mingyuanyun\Core\SDK;
use Mingyuanyun\Core\Support\RequestOptions;
use Mingyuanyun\Core\Support\RetryRule;
use PHPUnit\Framework\TestCase;

class SenderTest extends TestCase
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $config;

    protected function setUp(): void
    {
        $retryRule = new RetryRule();
        $retryRule->retryByClient(1, ['Connection timed out', 'Operation timed out']);
        $retryRule->retryByServer(2, ['系统繁忙'], [502]);

        $request = new Request(RequestOptions::METHOD_GET, '/api');
        $request->setRetryRule($retryRule);
        $request->pending();
        
        $this->request = $request;

        $this->config = [
            'secretId'  => 'mingyuanyun_secret_id_123',
            'secretKey' => 'mingyuanyun_secret_key_666',
            'debug'     => true,
            'server' => [
                'host' => 'https://www.domain.com',
            ],
        ];

        SDK::loadConfig($this->config);
    }

    protected function tearDown(): void
    {
        $this->request = null;
        SDK::clearConfig();
        HttpSender::cancelMock();
    }
    
    // 同步请求
    // 发起一个正常请求，需要能成功
    public function test_should_send_request_success()
    {
        HttpSender::mockResponse(200, [], ['code' => 0, 'message' => 'success']);
        $sender = HttpSender::mock();

        $response = $sender->send($this->request);

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals(0, $response->getBody('code'));
        $this->assertEquals('success', $response->getBody('message'));
    }
    
    // 遇到网络问题时，自动触发请求重试
    public function test_should_auto_retry_with_network_anomaly()
    {
        HttpSender::mockRequestException(
            'cURL error 28: Operation timed out after 500 milliseconds with 0 out of 0 bytes received',
            new Psr7Request('GET', $this->request->getUri())
        );
        HttpSender::mockResponse(200, ['Content-Type' => 'application/json'], ['code' => 0, 'message' => 'success']);

        $sender = HttpSender::mock();

        $response = $sender->send($this->request);

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertTrue($response->isJson());
        $this->assertEquals(0, $response->getBody('code'));
        $this->assertEquals('success', $response->getBody('message'));
        $this->assertEquals(1, $response->getClientRetryTimes());
        $this->assertEquals(0, $response->getServerRetryTimes());
    }

    // 遇到网络问题时，超出重试次数仍请求失败，应能正常抛出异常
    public function test_should_throw_exception_if_retry_limit_was_exceeded()
    {
        HttpSender::mockRequestException(
            'cURL error 28: Connection timed out after 500 milliseconds with 0 out of 0 bytes received',
            new Psr7Request('GET', $this->request->getUri())
        );
        HttpSender::mockRequestException(
            'cURL error 28: Operation timed out after 1000 milliseconds with 0 out of 0 bytes received',
            new Psr7Request('GET', $this->request->getUri())
        );

        $sender = HttpSender::mock();

        $this->expectException(ClientException::class);
        $this->expectExceptionMessageMatches('/Operation timed out/');
        
        $sender->send($this->request);
    }
    
    // 如果服务端返回的失败信息能匹配上重试规则，应该触发请求自动重试
    public function test_should_auto_retry_if_server_return_message_match_retry_rule()
    {
        HttpSender::mockResponse(502, [], '系统繁忙，请稍后再试');
        HttpSender::mockResponse(200, [], ['code' => 0, 'message' => 'success']);

        $sender = HttpSender::mock();

        $response = $sender->send($this->request);

        $this->assertNotNull($response);
        $this->assertTrue($response->isSuccess(true));
        $this->assertEquals(0, $response->getBody('code'));
        $this->assertEquals('success', $response->getBody('message'));
        $this->assertEquals(0, $response->getClientRetryTimes());
        $this->assertEquals(1, $response->getServerRetryTimes());
    }

    // 如果服务端多次返回能匹配上重试规则的失败信息，超出重试次数后，应该能正常返回失败结果
    public function test_should_return_faild_info_if_retry_limit_was_exceeded()
    {
        HttpSender::mockResponse(502, [], '系统繁忙，请稍后再试');
        HttpSender::mockResponse(504, [], '系统繁忙，请求超时');
        HttpSender::mockResponse(504, [], '系统繁忙，请求超时');

        $sender = HttpSender::mock();

        $response = $sender->send($this->request);

        $this->assertNotNull($response);
        $this->assertFalse($response->isSuccess());
        $this->assertEquals(504, $response->getStatusCode());
        $this->assertEquals('系统繁忙，请求超时', $response->toString());
        $this->assertEquals(0, $response->getClientRetryTimes());
        $this->assertEquals(2, $response->getServerRetryTimes());
    }

    // 如果请求中携带有超时时间设置，发送器应该以请求的值为准
    public function test_should_use_request_timeout_value_if_it_provided()
    {
        $request = new Request(RequestOptions::METHOD_GET, 'https://www.google.com');
        $request->setConnectTimeout(0.5);
        $request->pending();

        $sender = new HttpSender;
        $options = $sender->getOptions();

        $this->expectException(ClientException::class);

        $st = microtime(true);
        $sender->send($request);
        $et = microtime(true);

        $cost = $et - $st;

        $this->assertLessThan($options['connect_timeout'], $cost);
        $this->assertGreaterThanOrEqual(0.5, $cost);
    }
}